% Comparison of whittle approximation with non-uniform sampling

% number of grid points
N = 1000;

% number of frequencies to use
Nfreq = 40;

% sampling locations in [0, lambda]
lambda = 40;

% set true lengthscale 
lengthscales = [.1 1 10];

% Number of simulations to conduct
Nsims = 1000;

biases = {'SubbaRao', 'MatsudaY', 'naiveDFT'};
results = struct();

%% 2. Randomly chosen sampling

t = sort(lambda * rand(N, 1));


for j=1:length(lengthscales)
    theta = lengthscales(j);
    fprintf('lengthscale = %0.2f \n', theta);
    results(j).lengthscale = theta;
    for bias = biases
        b = bias{1};
        res = zeros(Nsims, 2);
        for s = 1:Nsims
            res(s, :) = compare_whittle_nospatial(t, lambda, Nfreq, theta, b);
            if res(s,1) > 1e5
                res(s,1) = Inf;
            end
        end
        results(j).(b) = res;
        fprintf('%s method: median %.02f, stddev %0.2f \n', ...
            b, median(res(:,1), 1), sqrt(var(res(:,1))));
    end
end