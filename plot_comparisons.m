% Comparison of whittle approximation, tapered whittle, and true

% set random seed
r_seed = 123;

% number of grid points
N = 1000;

% sampling locations in [0, lambda]
lambda = 40;

% set true lengthscale 
lengthscale = 10;

% For the figures
screensize = get(groot, 'ScreenSize');
wd = screensize(3);
ht = screensize(4);

%% 1. Equally spaced sampling grid

t = lambda * (1:N).' / N;

% number of frequencies to use
Nfreq = N;

% run the comparison
figure('Name', 'Equally spaced locations', ...
    'Position', [1 ht*.2 wd/2 ht/2])
compare_whittle(t, lambda, Nfreq, lengthscale, 'none', r_seed)
saveas(gcf, 'figures/equally_spaced', 'png')


%% 2. Randomly chosen sampling with Matsuda-Yajima bias correction

t = sort(lambda * rand(N, 1));

Nfreq = 30; 

figure('Name', 'randomly sampled locations w/Matsuda-Yajima bias correct',...
    'Position', [wd*.1 ht*.3 wd/2 ht/2])
compare_whittle(t, lambda, Nfreq, lengthscale, 'MatsudaY', r_seed)
saveas(gcf, 'figures/Matsuda_Yajima_spaced', 'png')

%% 3. Randomly chosen sampling with Subba-Rao bias correction

t = sort(lambda * rand(N, 1));

Nfreq = 30; 

figure('Name', 'randomly sampled locations w/Subba-Rao bias correct',...
    'Position', [wd*.2 ht*.4 wd/2 ht/2])
compare_whittle(t, lambda, Nfreq, lengthscale, 'SubbaRao', r_seed)
saveas(gcf, 'figures/SubbaRao', 'png')

%% 4. Randomly chosen sampling, but DFT assumes equally spaced grid instead

t = sort(lambda * rand(N, 1));

Nfreq = N; 

figure('Name', 'randomly sampled locations with naive DFT',...
    'Position', [wd*.3 ht*.5 wd/2 ht/2])
compare_whittle(t, lambda, Nfreq, lengthscale,'naiveDFT', r_seed)
saveas(gcf, 'figures/NaiveDFT', 'png')
