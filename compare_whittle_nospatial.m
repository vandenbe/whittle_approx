function theta_hats = compare_whittle_nospatial(...
                        t, lambda, Nfreq, lengthscale, biastype)

assert(any(strcmp(biastype, {'none', 'SubbaRao', 'MatsudaY', 'naiveDFT'})))                    
                    
N = length(t);

% matrix of |t_i - t_j|
dists = abs(repmat(t, 1, N) - repmat(t.', N, 1));

% Covariance matrix

Cf = @(theta) exp(-dists / (theta)); % + .00001 * eye(N);
C = Cf(lengthscale);

% spectral density of continuous process
f_old = @(w, theta) 2 ./ (1/theta + 4 * (pi*w).^2 * theta);

% approximate spectral density of aliased process
f_delta = @(w, theta) f_old(w, theta) + f_old(w + N/lambda, theta) + f_old(w - N/lambda, theta) ...
     + f_old(w + 2*N/lambda, theta) + f_old(w - 2*N/lambda, theta);

 
%% Irregular sampling: add asymptotic bias to theoretical spectral density.
% Using Matsuda-Yajima bias correction.
c_k = lambda / (N);
if (max(diff(t)) - min(diff(t)) > 1e-8) && strcmp(biastype, 'MatsudaY')
    
    f_delta = @(w, theta) f_old(w, theta) + c_k;
end

%% ------data ----
x = chol(C, 'lower') * randn(N, 1);

% John Tukey's taper function
taper = tukeywin(N, .2);

% tapered data
xt = x .* taper;

%% Now compute periodogram

% discrete spectral grid
omegas = ((-Nfreq/2 + 1):(Nfreq/2)).' / lambda;

if strcmp(biastype, 'naiveDFT')
    % Force naive DFT assuming t's are equally spaced.
    T = lambda * (1:N).' / N;
else
    T = t;
end

% compute (non)-uniform Fourier transform of data
J = sqrt(lambda) / N * sum(repmat(x, 1, Nfreq) ...
    .* exp(-2*pi*1i * (T * omegas.')), 1).';

Jtaper = sqrt(lambda) / N * sum(repmat(xt, 1, Nfreq) ...
    .* exp(-2*pi*1i * (T * omegas.')), 1).';

pxx = abs(J).^2;
pxxt = abs(Jtaper).^2;
% Equally spaced case: [pxx, w] = periodogram(x, [], N, Fs, 'centered');
%                      [pxxt, wt] = periodogram(xt, [], N, Fs, 'centered');

% Irregular sampling:
% Remove bias from periodogram using section 4.1 of Subba Rao 2016
if (max(diff(t)) - min(diff(t)) > 1e-8) && strcmp(biastype, 'SubbaRao')
    pxx = pxx - lambda / (N);
    pxxt = pxxt - lambda / (N);
end



%% log likelihood functions
whit = @(theta)  - .5 * sum(log(f_delta(omegas, theta)) + pxx ./ f_delta(omegas, theta));
whit_taper = @(theta) - .5 * sum(log(f_delta(omegas, theta)) + pxxt ./ f_delta(omegas, theta));

% Find maximums for lengthscale parameter
options = optimoptions('fminunc', 'display', 'off', 'algorithm', 'quasi-newton');

lmax =  fminunc(@(x) -whit(x), lengthscale, options);
lmax_taper = fminunc(@(x) -whit_taper(x), lengthscale, options);

theta_hats = [lmax, lmax_taper];

end