function theta_hats = compare_whittle(...
                        t, lambda, Nfreq, lengthscale, biastype, r_seed)

assert(any(strcmp(biastype, {'none', 'SubbaRao', 'MatsudaY', 'naiveDFT'})))

% set random_number gen
if nargin > 5
    rng(r_seed);
end

N = length(t);

% matrix of |t_i - t_j|
dists = abs(repmat(t, 1, N) - repmat(t.', N, 1));

% Covariance matrix

Cf = @(theta) exp(-dists / (theta)); % + .00001 * eye(N);
C = Cf(lengthscale);

% spectral density of continuous process
f_old = @(w, theta) 2 ./ (1/theta + 4 * (pi*w).^2 * theta);

% approximate spectral density of aliased process
f_delta = @(w, theta) f_old(w, theta) + f_old(w + N/lambda, theta) + f_old(w - N/lambda, theta) ...
     + f_old(w + 2*N/lambda, theta) + f_old(w - 2*N/lambda, theta);

 
%% Irregular sampling: add asymptotic bias to theoretical spectral density.
% Using Matsuda-Yajima bias correction.
c_k = lambda / (N);
if (max(diff(t)) - min(diff(t)) > 1e-8) && strcmp(biastype, 'MatsudaY')
    f_delta = @(w, theta) f_old(w, theta) + c_k;
end

%% ------data ----
x = chol(C, 'lower') * randn(N, 1);

% John Tukey's taper function
taper = tukeywin(N, .2);

% tapered data
xt = x .* taper;

%% Create four plots
subplot(2,2,1)
plot(t, x)
hold on
plot(t, xt)
xlabel('t (seconds)')
title(sprintf(...
    'sample from the exponential covariance\n process w/ lengthscale = %.2f', lengthscale))
legend('original', 'tapered', 'Location', 'best')


%% Now compute periodogram

% discrete spectral grid
omegas = ((-Nfreq/2 + 1):(Nfreq/2)).' / lambda;

if strcmp(biastype, 'naiveDFT')
    % Force naive DFT assuming t's are equally spaced.
    T = lambda * (1:N).' / N;
else
    T = t;
end

% compute (non)-uniform Fourier transform of data
J = sqrt(lambda) / N * sum(repmat(x, 1, Nfreq) ...
    .* exp(-2*pi*1i * (T * omegas.')), 1).';

Jtaper = sqrt(lambda) / N * sum(repmat(xt, 1, Nfreq) ...
    .* exp(-2*pi*1i * (T * omegas.')), 1).';

pxx = abs(J).^2;
pxxt = abs(Jtaper).^2;
%[pxx, w] = periodogram(x, [], N, Fs, 'centered');
%[pxxt, wt] = periodogram(xt, [], N, Fs, 'centered');

% Irregular sampling:
% Remove bias from periodogram using section 4.1 of Subba Rao 2016
if (max(diff(t)) - min(diff(t)) > 1e-8) && strcmp(biastype, 'SubbaRao')
    pxx = pxx - lambda / (N);
    pxxt = pxxt - lambda / (N);
end

subplot(2,2,2)
semilogy(omegas, smooth(pxx, 7))
hold on
semilogy(omegas, smooth(pxxt, 7))

semilogy(omegas, f_delta(omegas, lengthscale), 'LineWidth', 2);
xlabel('w (Hz)')
title(sprintf('smoothed periodograms and spectral density\n using %d frequencies', Nfreq))
legend('I(w)', 'tapered I(w)', 'spectral density f(w)', 'Location', 'best')


%% Plot I(w) / f(w)
subplot(2,2,3)
plot(omegas, smooth(pxx ./ f_delta(omegas, lengthscale), 9))
hold on;
plot(omegas, smooth(pxxt ./ f_delta(omegas, lengthscale), 9))
plot(omegas, ones(length(omegas), 1))
title('smoothed I(w) / f(w)')
xlabel('w (Hz)')
legend('original I(w)', 'tapered I(w)', 'Location', 'best')

%% Plot log likelihood functions and their optima
% log likelihood functions
whit = @(theta)  - .5 * sum(log(f_delta(omegas, theta)) + pxx ./ f_delta(omegas, theta));
whit_taper = @(theta) - .5 * sum(log(f_delta(omegas, theta)) + pxxt ./ f_delta(omegas, theta));
loglik = @(theta) - sum(log(abs(diag(chol(Cf(theta), 'lower'))))) - .5 * x.' * ((Cf(theta)) \ x);

% Find maximums for lengthscale parameter
options = optimoptions('fminunc', 'display', 'off', 'algorithm', 'quasi-newton');

lmax =  fminunc(@(x) -whit(x), lengthscale, options);
lmax_taper = fminunc(@(x) -whit_taper(x), lengthscale, options);
lmax_true = fmincon(@(x) -loglik(x), lengthscale, [], [], [], [], ...
    lengthscale/5, lengthscale*5);

ax4 = subplot(2,2,4);
%semilogy(omegas, f(omegas, lengthscale));

lscales = linspace(lengthscale/10,lengthscale*5, 100);

% subtract off log like max value to make easy visual comparison
plot(lscales, arrayfun(whit, lscales) - whit(lmax))
hold on
plot(lscales, arrayfun(whit_taper, lscales) - whit_taper(lmax_taper))
plot(lscales, arrayfun(loglik, lscales) - loglik(lmax_true))

ax4.YLim = [-30 10];
xlabel('\theta -- lengthscale')
title('log likelihood of lengthscale parameter')
legend('whittle', 'tapered', 'spatial (true)', 'Location', 'best')

theta_hats = [lmax, lmax_taper, lmax_true];

scatter(lmax, 0) %whit(lmax))
scatter(lmax_taper, 0)%whit_taper(lmax_taper))
scatter(lmax_true, 0)%loglik(lmax_true))

end